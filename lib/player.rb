class HumanPlayer
  attr_reader :name, :ships
  attr_accessor :board

  def initialize(name, board = Board.default_grid)
    @name = name
    @board = board
    @ships = [
      Ship.new(self, "Destroyer", 2),
      Ship.new(self, "Cruiser", 3)
    ]
  end

  def get_play
    print "#{name}, where would you like to fire? (i.e. '0 2'): "
    pos = gets.chomp.split

    until valid_play?(pos)
      pos = get_valid_play
    end

    pos.map(&:to_i)
  end

  def valid_play?(pos)
    # Check that the player input two numbers
    pos.length == 2 && pos.all? { |ch| ch.to_i.to_s == ch }
  end

  def get_valid_play
    print "Please enter a valid move (i.e. '0 0' for top-left corner): "
    gets.chomp.split
  end

  def setup
    display
    puts "#{name}, you will now place your ships on your board."
    place_ships

    display(false)

    sleep(5)
    system "clear"
  end

  def valid_setup?(pos)
    return false unless valid_play?(pos)

    pos = pos.map(&:to_i)
    board.in_range?(pos) && board[pos] != :s
  end

  def get_valid_setup(pos)
    if !valid_play?(pos)
      puts "Moves must be in the form 'row col' (i.e. '0 0' for top-left)"
    elsif !board.in_range?(pos.map(&:to_i))
      puts "That position is not on the grid! Choose again."
    elsif board[pos.map(&:to_i)] == :s
      puts "You already placed a ship in that position! Choose again."
    end
    puts

    print "Please enter a valid position: "
    gets.chomp.split
  end

  def display(hidden = true)
    board.display(hidden)
  end

  def display_opp_board(opp_board)
    opp_board.display
  end

  private

  def place_ships
    ships.each do |ship|
      puts "Placing your #{ship.name} (length: #{ship.size})"

      print "Starting position: "
      start_pos = gets.chomp.split.map(&:to_i)

      print "Ending position (should be #{ship.size} spots from start): "
      end_pos = gets.chomp.split.map(&:to_i)

      ship.place(board, start_pos, end_pos)
      puts
    end
    # num.times do |i|
    #   print "Position of ship ##{i + 1}?: "
    #   pos = gets.chomp.split
    #
    #   until valid_setup?(pos)
    #     pos = get_valid_setup(pos)
    #   end
    #
    #   board[pos.map(&:to_i)] = :s
    # end
  end

end

class ComputerPlayer
  attr_reader :name
  attr_accessor :board, :opponent_board

  def initialize(name, board = Board.default_grid)
    @name = name
    @board = board
  end

  def get_play
    print "#{name}, where would you like to fire? (i.e. '0 2'): "
    pos = opponent_board.unmarked_spaces.sample

    sleep(1)
    puts "#{pos[0]} #{pos[-1]}"
    sleep(1)

    pos
  end

  def setup(num)
    display
    puts "#{name} is placing #{num} ships..."
    sleep(3)
    place_ships(num)

    display(false)
    sleep(5)

    system "clear"
  end

  def display(hidden = true)
    board.display(hidden)
  end

  def display_opp_board(opp_board)
    @opponent_board = opp_board
    opp_board.display
  end

  private

  def place_ships(num)
    num.times do |_|
      pos = board.empty_spaces.sample
      board[pos.map(&:to_i)] = :s
    end
  end

end
