class Board
  attr_reader :grid, :rows, :cols

  def initialize(grid = Board.default_grid)
    @grid = grid
    @rows = grid.length
    @cols = grid.first.length
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def count
    grid.reduce(0) { |accum, row| accum + row.count(:s) }
  end

  def empty?(pos = nil)
    pos.nil? ? count == 0 : self[pos] == nil
  end

  def full?
    grid.none? { |row| row.include?(nil) }
  end

  def won?
    count == 0
  end

  def all_spaces
    all_spaces = []

    (0...rows).each do |row|
      (0...cols).each do |col|
        all_spaces << [row, col]
      end
    end

    all_spaces
  end

  def empty_spaces
    all_spaces.select { |pos| self[pos].nil? }
  end

  def unmarked_spaces
    all_spaces.reject { |pos| self[pos] == :x }
  end

  def place_random_ship
    raise "The board is already full" if full?
    pos = empty_spaces.sample
    self[pos] = :s
  end

  def in_range?(pos)
    col, row = pos
    (0...cols).include?(col) && (0...rows).include?(row)
  end

  def display(hidden = true)
    print "     "
    cols.times { |i| print "  #{i} " }
    print "\n"

    grid.each_with_index do |row, i|
      puts "     " + "-" * (cols * 4 + 1)
      print "  #{i}  "
      hidden_row(row) if hidden
      reveal_row(row) unless hidden
    end

    puts "     " + "-" * (cols * 4 + 1)
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    grid[row][col] = mark
  end

  private

  def hidden_row(row)
    row.each do |el|
      print el == :x ? "| x " : "|   "
    end
    print "|\n"
  end

  def reveal_row(row)
    row.each do |el|
      print el.is_a?(Ship) ? "| s " : "|   "
    end
    print "|\n"
  end

end

if __FILE__ == $PROGRAM_NAME
  board = Board.new(Array.new(5) { Array.new(5) })
  10.times { board.place_random_ship }
  board.display
end
