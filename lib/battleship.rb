require_relative "board"
require_relative "player"
require_relative "ship"

class BattleshipGame
  attr_reader :current_player, :opponent
  attr_accessor :board

  def initialize(player1, player2)
    # @player1 = player1
    # @player2 = player2
    @current_player = player2
    @opponent = player1
    @board = player2.board
  end

  def attack(pos)
    puts
    puts board[pos] == :s ? "YOU HIT A SHIP!!" : "YOU MISSED!!"
    puts
    sleep(1)
    board[pos] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def switch_player
    @current_player, @opponent = opponent, current_player
    puts "#{current_player.name}'s turn!"
    @board = opponent.board
  end

  def display_status
    puts "Remaining ships: #{count}"
    puts
    current_player.display_opp_board(board)
    puts
  end

  def play_turn
    display_status
    pos = current_player.get_play

    until valid_turn?(pos)
      pos = get_valid_turn(pos)
    end

    attack(pos)
    system "clear"
  end

  def valid_turn?(pos)
    board.in_range?(pos) && board[pos] != :x
  end

  def get_valid_turn(pos)
    if !board.in_range?(pos)
      puts "That position is not on the grid! Choose again."
    elsif board[pos] == :x
      puts "You already fired at that space! Choose again."
    end
    puts

    current_player.get_play
  end

  def conclude
    puts "*****************"
    print "CONGRATULATIONS, #{current_player.name.upcase}!!! "
    puts "YOU SUNK ALL THE SHIPS!!!"
    puts "*****************"
  end

  def setup
    opponent.setup
    current_player.setup(3)
  end

  def play
    setup

    until game_over?
      switch_player
      play_turn
    end

    conclude
  end

end

if __FILE__ == $PROGRAM_NAME
  board1 = Board.new(Array.new(4) { Array.new(4) })
  board2 = Board.new(Array.new(4) { Array.new(4) })

  player1 = HumanPlayer.new("Kyle", board1)
  player2 = ComputerPlayer.new("Wall-E", board2)

  game = BattleshipGame.new(player1, player2)
  game.play
end
