require_relative "player"
require_relative "board"

class Ship
  attr_reader :player, :name, :size
  attr_accessor :positions

  def initialize(player, name, size)
    @player = player
    @name = name
    @size = size
    @positions = []
  end

  def sunk?
    positions == []
  end

  def hit(board, pos)
    positions.delete(pos)
    board[pos] = :s
  end

  def place(board, start_pos, end_pos)
    start_row, start_col = start_pos
    end_row, end_col = end_pos

    if start_row == end_row
      # ship is placed horizontally
      (start_col..end_col).each { |col| positions << [start_row, col] }
    else
      # ship is placed vertically
      (start_row..end_row).each { |row| positions << [row, start_col] }
    end

    positions.each { |pos| board[pos] = self }
  end

end

if __FILE__ == $PROGRAM_NAME
  board1 = Board.new(Array.new(4) { Array.new(4) })
  player1 = HumanPlayer.new("Kyle", board1)
  ship1 = Ship.new(player1, "battleship", 3)

  ship1.place([0, 0], [0, 1])
  puts ship1.sunk?
  ship1.hit([0,0])
  ship1.hit([0,1])
  ship1.hit([0,2])
  puts ship1.sunk?
end
